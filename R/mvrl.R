#' Function to calculate ylim for plotting density on histograms
#'
#' @param x A vector of data for wich a histogram with a density line is to be plotted
#' @param decimals Number of decimal places for rounding the upper limit.
#' @return A vector representing the limits of the y axis
#' @export
get_density_limits <- function(x, decimals=2) {
  f <- 10^decimals
  return(c(floor(min(density(x)$y)), ceiling(max(density(x)$y*f*1.005))/f))
}

bcomma <- function(x) format(x, big.mark = ".", decimal.mark ="," , scientific = FALSE) 
