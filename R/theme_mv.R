#' Function to get default font family 
#'
#' @return A font family name
get_default_font <- function() {
  extrafont::choose_font(c("Open Sans", "Helvetica"))
}


#' ggplot2theme
#' @param base_size Base fontsize for theme (default = 12)
#' @param base_family Default family used for plot. Defaults to
#' FlandersArtSans, with a fallback to Helvetica.
#' @export
theme_mv <- function(base_size = 12, base_family = get_default_font()) {
  ggplot2::update_geom_defaults("bar", list(colour="black", fill = "blue", alpha=1/2))
  ggplot2::update_geom_defaults("line", list(colour='red'))
  ggplot2::theme_bw(base_family=base_family)
}


#' Function to return a default pdf graphics device
#'
#' @param file Output filename
#' @param width Defaults to 9
#' @param height Defaults to 7
#' @return A pdf graphics device
#' @export
mv_pdf <- function(file='output.pdf', width=9, height=7) {
  grDevices::pdf(file, width, height, onefile=TRUE, family=get_default_font(), colormodel='cmyk')
}

#' Function to embed fonts
#'
#' @export
mv_embed_fonts <- extrafont::embed_fonts
