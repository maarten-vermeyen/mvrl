README
======

This R package contains a ggplot2 theme and some helper functions. The 
theme is intended to use the Open Sans font, installed via extrafont. If this font isn't available, the package falls back on Helvetica.

Installation
------------

To install this library directly from GitHub:

```R
library(devtools)
install_github('OnroerendErfgoed/mvrl', auth_token = '<PAT_TOKEN>')
```

The PAT token is only required untill we make the package publicly available.
